"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UseFetch = void 0;
const react_1 = require("react");
const UseFetch = () => {
    const [loading, setLoading] = (0, react_1.useState)(false);
    const [error, setError] = (0, react_1.useState)(null);
    const request = (0, react_1.useCallback)((url, method = 'GET', body = null, headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }) => __awaiter(void 0, void 0, void 0, function* () {
        setLoading(true);
        try {
            const responce = yield fetch(url, method, body, headers = {});
            const data = responce.json();
            if (!responce.ok) {
                setError(data.message);
                throw new Error(data.message || "Error conection to data");
            }
            setLoading(false);
            return data;
        }
        catch (e) {
            setLoading(false);
            setError(e.message);
            throw e;
        }
    }), []);
    const clearError = () => setError(null);
    return { loading, request, error, clearError };
};
exports.UseFetch = UseFetch;
