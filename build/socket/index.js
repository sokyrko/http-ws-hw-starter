"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const game_1 = __importDefault(require("./game"));
const login_1 = __importDefault(require("./login"));
exports.default = (io) => {
    io.on('connection', socket => {
        const username = socket.handshake.query.username;
        console.log(username, 'Connected');
    });
    (0, game_1.default)(io.of("/game"));
    (0, login_1.default)(io.of("/login"));
};
