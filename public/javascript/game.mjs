import {showInputModal, showMessageModal, showResultsModal} from './views/modal.mjs';
import  { appendUserElement, changeReadyStatus, setProgress, removeUserElement } from './views/user.mjs';
import  { appendRoomElement, updateNumberOfUsersInRoom, removeRoomElement } from './views/room.mjs';
import {createElement} from "./helpers/domHelper.mjs";
// import {json} from "express";
// import {UseFetch} from '../../src/hooks/fetch.hooks.js';

const username = sessionStorage.getItem('username');
const userId = sessionStorage;
const addRoomBtn = document.querySelector("#add-room-btn");
const quitRoomBtn = document.querySelector('#quit-room-btn')
const readyBtn = document.querySelector('#ready-btn')

const roomsPage = document.querySelector("#rooms-page");
const gamePage = document.querySelector('#game-page')
const gameContainer = document.querySelector('#game-container')
// const {request} = UseFetch();
let  randText =[]
const getRoomName = document.querySelector('data-room-name');
let currentUser ={user_name: username, user_status: false};
let secBeforeGame = null

let users = [];
let rooms =[];
let roomName = null;

let progress = 0
let cnt = 0
const socket = io("http://localhost:3002/game");

	socket.emit("CONNECTED",  {name: username})


const onJoin = (payload) => {
	socket.emit('JOIN_TO_ROOM', payload)
	gamePage.classList.remove('display-none')
	roomsPage.classList.add('display-none')
	const blockTime  = document.createElement('span')


}
const readyStatus = () =>{

	const curUser = users.find(user=> user.name=== username)
	socket.emit('CHANGE_USER_STATUS',{user_name : username, user_status: !curUser.user_status})

	users.forEach(user=>{
		if (user.room_name=== curUser.room_name){
			changeReadyStatus({ username:user.name, ready: user.user_status })

			user.user_status ? readyBtn.innerHTML="READY" : readyBtn.innerHTML="NOTREADY"

		}
	})

}
const changeGameContainer = (e)=>{
	const allSymbos = randText.length
		if(randText[cnt]===e.key){
			cnt = cnt + 1
			const strBefore = randText.slice(0,cnt-1).join("")+""
			const strUnderline = cnt < randText.length ? randText[cnt]: ""
			const strAfter = cnt < randText.length ? randText.slice(cnt+1).join("")+"" : ""
			let strRes = `<span style="background-color: lime">${strBefore}$</span>
							<span style="text-decoration: underline">${strUnderline}</span>
							<span>${strAfter}</span>`
			if (progress===100) {
				strRes = `<span style="background-color: green">${strBefore}$</span>`

			}

			gameContainer.innerHTML=strRes
			progress = cnt*100/ allSymbos
			socket.emit("SET_PROGRESS", {user_name: username, user_progress:progress})

		}

}

const closeModalResult = () =>{
	const curUser = users.find(user=> user.name=== username)

	quitRoomBtn.classList.remove('display-none')
	gameContainer.querySelector('#text-container').classList.add('display-none')
	gameContainer.querySelector('#text-container').innerHTML=""
	gameContainer.querySelector('#game-timer').classList.add('display-none')
	readyBtn.classList.remove('display-none')
	readyBtn.innerHTML='READY'

	users.forEach(user=>{
		if(user.room_name === curUser.room_name){
			socket.emit('CANCEL_USER_STATUS',{user_name : user.name, user_status: false, user_progress: 0})
		}
	})
}
socket.on("SEC_BEFORE_START", payload=>{

	secBeforeGame = payload.secund
	quitRoomBtn.classList.add('display-none')
	readyBtn.classList.add('display-none')
	gameContainer.querySelector('#text-container').classList.remove('display-none')

	if(secBeforeGame) gameContainer.querySelector('#text-container').innerHTML= secBeforeGame
	if(secBeforeGame===0) {
		socket.on("SEND_ID_TEXT", payload=>{
			let remainTime =  payload.SECONDS_FOR_GAME
			socket.emit("GET_ID_TEXT", {idText: payload.idText})
			socket.on("SEND_TEXT",payload=>{
				randText = [...payload.text]
				gameContainer.querySelector('#text-container').innerHTML=payload.text
			})
			document.querySelector('#game-page').addEventListener('keypress', changeGameContainer)
				const endInterval = setInterval( () => {
				remainTime = remainTime - 1
					gameContainer.querySelector('#game-timer').classList.remove('display-none')
					gameContainer.querySelector('#game-timer-seconds').innerHTML= `${remainTime}`

					//end the game
				if (remainTime === 0) {
					document.querySelector('#game-page').removeEventListener('keypress',changeGameContainer)
					const usersSortedArray = users.sort((a,b)=> {
						return a.user_progress - b.user_progress
					})
					const sArray = usersSortedArray.map(user=> user.name)
					showResultsModal({usersSortedArray: sArray , onClose:closeModalResult })
					clearInterval(endInterval)
				}
				//end block finish game

			},1000)
		})
	}


})


const quitRoom = () =>{
	socket.emit('EXIT_FROM_ROOM',{user_name : username})
	gamePage.classList.add('display-none')
	roomsPage.classList.remove('display-none')
}


socket.on("ALL_USER", payload => {
	console.log(payload);
	users = payload
	const curUser = payload.find(user=> user.name=== username)
	console.log(curUser);
	if(curUser.room_name) {
		const jUsers = users.filter(user => user.room_name === curUser.room_name)
		const renderUser = document.querySelector(`[data-username=${curUser.name}]`)
		document.querySelector('#users-wrapper').innerHTML=""
		jUsers.forEach( user => {
			appendUserElement({
				username: user.name,
				ready: user.user_status,
				isCurrentUser: user.name === username ? true : false
			})
			setProgress ({ username:user.name, progress:user.user_progress })

		})
	}
})

socket.on("ALL_ROOM", payload => {
	if (payload.length) rooms= payload;
	const roomsContainer = document.querySelector('#rooms-wrapper');
	roomsContainer.innerHTML='';
	rooms.forEach(
		room => appendRoomElement({
			name:  room.name,
			numberOfUsers: room.joined,
			onJoin : () => {
				onJoin({room_name:room.name, user_name: username })
			}
		})
	)
} );

//add room
const addRoom = () => {
	showInputModal({
		title: "Title of room",
		onChange: (e) =>{
			roomName= e;
		},
		onSubmit: () => {
			const checkRoom = rooms.find(room => room.name=== roomName)
			if (checkRoom){
				showMessageModal({
									message: "Room already exist", onClose: () => {
									}
								})
			} else{
				socket.emit("ADD_ROOM", {name: roomName})
			}

		}

	})
}
addRoomBtn.addEventListener('click',addRoom)
quitRoomBtn.addEventListener('click',quitRoom)
readyBtn.addEventListener('click',readyStatus)
// gameContainer.parentNode.addEventListener('change',changeGameContainer )