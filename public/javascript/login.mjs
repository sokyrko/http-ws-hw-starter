import {showMessageModal} from "./views/modal.mjs";
sessionStorage.clear();
const username = sessionStorage.getItem('username');
const socket = io("http://localhost:3002/login");
// const game_spn = io("http://localhost:3002/game");

if (username) {

	window.location.replace('/game');
}

const submitButton = document.getElementById('submit-button');
const input = document.getElementById('username-input');

const getInputValue = () => input.value;

const onClickSubmitButton = () => {
	const inputValue = getInputValue();
	if (!inputValue) {
		return;
	}

	new Promise((resolve, reject) => {
		socket.emit("ADD_USER", inputValue);
	}).then(
		socket.on("CHECK_USER", (payload) => {
			if (payload === true) {
				showMessageModal({
					message: "User already exist", onClose: () => {
					}
				})
			}
			else
			{
				sessionStorage.setItem('username', inputValue);
				window.location.replace('/game');
			}
		})
	)
};

const onKeyUp = ev => {
	const enterKeyCode = 13;
	if (ev.keyCode === enterKeyCode) {
		submitButton.click();
	}
};

submitButton.addEventListener('click', onClickSubmitButton);
window.addEventListener('keyup', onKeyUp);
