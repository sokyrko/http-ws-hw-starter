import {MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_TIMER_BEFORE_START_GAME,SECONDS_FOR_GAME } from './config'
import {texts} from '../data';

const rooms = [{name:"Rooms1", joined: 0}];
let users = [];
let joins= [];
let roomId = 0;
let roomsWithConnect = [];

const checkId = (payload, socket ) => {
    let check = users.find(user=>user.name===payload.name)
    if(!check) {
        // check = {...check, user_id: socket.id}
    // } else {
        users.push({ name: payload.name, user_status: false, room_name: null, user_progress:0})
    }
}
export  default io => {
    io.on("connection", socket => {
        socket.on('CONNECTED',(payload)=>{
            checkId(payload, socket);
            socket.emit("ALL_USER", users);
            socket.emit("ALL_ROOM", rooms);
            socket.broadcast.emit("ALL_USER", users);
            socket.broadcast.emit("ALL_ROOM", rooms);
        })

        socket.on("SET_PROGRESS", payload=>{
            const userProgress = users.find(user => user.name === payload.user_name)
            userProgress.user_progress = payload.user_progress
            socket.emit("ALL_USER", users)
            socket.broadcast.emit("ALL_USER", users)
        })
        socket.on('CANCEL_USER_STATUS', payload => {
                const curUser = users.find(user => user.name === payload.user_name)
                curUser.user_status = payload.user_status
                curUser.user_progress = payload.user_progress
                socket.emit("ALL_USER", users)
                socket.broadcast.emit("ALL_USER", users)
            }
        )


        //changed status only for current user
        socket.on("CHANGE_USER_STATUS", payload => {
            const connectedUser = users.find(user => user.name === payload.user_name);
            connectedUser.user_status = payload.user_status ;
            socket.emit("ALL_USER", users);
            socket.broadcast.emit("ALL_USER", users);
            //start timer
            const checkStartAllUsers = users.filter(user=> user.user_status===true)
            if (checkStartAllUsers.length === MAXIMUM_USERS_FOR_ONE_ROOM){
                let secBeforeStart = SECONDS_TIMER_BEFORE_START_GAME
                const st = setInterval(()=>{
                    secBeforeStart = secBeforeStart -1
                    socket.emit("SEC_BEFORE_START", {secund: secBeforeStart})
                    socket.broadcast.emit("SEC_BEFORE_START", {secund: secBeforeStart})
                    if(secBeforeStart===-1) {
                        clearInterval(st)
                        const idText =  Math.round(Math.random(texts.length)*10)
                        socket.emit("SEND_ID_TEXT", {idText, SECONDS_FOR_GAME})
                        socket.broadcast.emit("SEND_ID_TEXT", {idText, SECONDS_FOR_GAME})
                    }
                },1000)
            }

        })
        socket.on("GET_ID_TEXT", payload=>{
            const text = texts[payload.idText-1]
            socket.emit("SEND_TEXT", {text: text})
        })
        //user join to rooom get rooms name  {room_name: room_name, user_name: user_name}
        socket.on("JOIN_TO_ROOM", payload => {
            const countJoinedToRoom = rooms.find(room => room.name === payload.room_name)
            if(countJoinedToRoom.joined >=MAXIMUM_USERS_FOR_ONE_ROOM){
                socket.emit("MAXIMUM_USERS_FOR_ONE_ROOM");
                return;
            }
            countJoinedToRoom.joined = countJoinedToRoom.joined + 1
            const joinedUser = users.find(user => user.name === payload.user_name);
            joinedUser.room_name=payload.room_name
            socket.emit('ALL_ROOM', rooms)
            socket.emit('ALL_USER', users)
            socket.broadcast.emit('ALL_ROOM', rooms)
            socket.broadcast.emit('ALL_USER', users)
        });

        //exit out room paylod = {user_name : user_name}
        socket.on("EXIT_FROM_ROOM" , (payload) => {
            const joinedUser = users.find(user => user.name === payload.user_name);
            if(!joinedUser) return;
            const room = rooms.find(room => room.name === joinedUser.room_name);
            if (!room) return
            room.joined = room.joined -1;
            joinedUser.room_name= null;
            socket.emit('ALL_ROOM', rooms)
            socket.emit('ALL_USER', users)
            socket.broadcast.emit('ALL_ROOM', rooms)
            socket.broadcast.emit('ALL_USER', users)
            socket.leave(joinedUser.room_name)
        })

        socket.on("ADD_ROOM", payload => {
            rooms.push({name: payload.name, joined: 0});
                socket.emit("ALL_ROOM", rooms);
                socket.broadcast.emit("ALL_ROOM", rooms);
        });

    });
};