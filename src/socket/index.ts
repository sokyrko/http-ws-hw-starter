import { Server } from 'socket.io';
import game  from "./game";
import login  from "./login";
import * as config from './config';

export default (io: Server) => {
	io.on('connection', socket => {
		const username = socket.handshake.query.username;
		console.log(username, 'Connected');
	});
	game(io.of("/game"));
	login(io.of("/login"));




};
