const users = [];

export  default io => {
    io.on("connection", socket => {

        socket.broadcast.emit("ALL_USER", users);
        socket.on("ADD_USER", username => {
            const isExist = users.find(user => user.user_name === username);
            if(isExist) {
                socket.emit("CHECK_USER", true);
                return;
            }
            socket.emit("CHECK_USER", false);
            users.push({user_id : socket.id, user_name: username, user_status: false });
        });

    });
};